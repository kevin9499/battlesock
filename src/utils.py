import json
import socket
import pygame


class Network:
    server = ""
    port = 1234
    address = server, port

    def __init__(
        self,
        sock=socket.socket(socket.AF_INET, socket.SOCK_STREAM),
    ):
        self.client = sock
        self.client.connect(self.address)

    def receive(self):
        buff = b""
        n = int.from_bytes(self.client.recv(4)[:4], "big")
        while n > 0:
            b = self.client.recv(n)
            buff += b
            n -= len(b)
        return json.loads(buff.decode())

    def send(self, *data):
        if len(data) == 1:
            data = data[0]
        final_data = b""
        data = json.dumps(data)
        final_data += len(data).to_bytes(4, "big")
        final_data += data.encode()
        try:
            self.client.send(final_data)
        except:
            pass

    def close(self):
        self.client.close()


def make_grid(sx, ex, sy, ey, color, size=35):
    return [
        [Node((x, y, size, size), color).__dict__ for y in range(sy, ey, size)]
        for x in range(sx, ex, size)
    ]


def image_at(sheet, rect):
    rect = pygame.Rect(*rect)
    image = pygame.Surface(rect.size).convert_alpha()
    image.blit(sheet, (0, 0), rect)
    image.set_colorkey((0, 0, 0))
    return image


SHIPS = {
    "Carrier": 5,
    "Battleship": 4,
    "Cruiser": 3,
    "Submarine": 3,
    "Destroyer": 2,
}


class Node:
    def __init__(self, rect, color):
        self.rect = rect
        self.ship = None
        self.color = color
        self.empty = 1
        self.aimed = False
        self.perma_color = None


class COLORS:
    RED = 239, 83, 80
    GREEN = 102, 187, 106
    WHITE = 250, 250, 250
    BLACK = 33, 33, 33
    BACKGROUND = 38, 50, 56
    DARK = 38, 50, 56
    AQUA = 38, 166, 154
    BLUE = 41, 182, 246
    CYAN = 38, 198, 218
    HOVER = 84, 110, 122
