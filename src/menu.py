# coding: utf-8
"""menu.py
src game menu.
author: Francois Lavigne Marbach
maintainers: 
    - Raphael Maria
    - Kevin Lefebvre

last modified: 29/11/2021
"""
# ---------------------
# ------ imports ------
# built in
import pygame
import random
from utils import COLORS


# class qui déssine les bateaux sur l'écran d'accueil
class Ship:
    def __init__(self):
        self.image = pygame.image.load("src/assets/ship.png")
        self.dir = "<"
        if random.choice((True, False)) == True:
            self.image = pygame.transform.flip(self.image, True, False)
            self.dir = ">"
        self.x = 450 if self.dir == "<" else -self.image.get_width()
        self.y = random.choice((random.randint(250, 320), random.randint(510, 650)))
        self.visible = True

    def draw(self, screen):
        self.x = self.x + 3 if self.dir == ">" else self.x - 3
        if self.x < -self.image.get_width() or self.x > 750 + self.image.get_width():
            self.visible = False
        screen.blit(self.image, (self.x, self.y))


# le menu du jeu qui permet de rejoindre / créer une partie
class Menu:
    def __init__(self, screen):
        self.font = pygame.font.Font("src/assets/futuristic_font_bold.otf", 36)
        self.small_font = pygame.font.Font("src/assets/futuristic_font.otf", 25)
        self.screen = screen
        self.load_entities()
        self.invalid_code = False
        self.show_menu = True
        self.game_taken = False
        self.particles = []
        self.ships = [Ship() for _ in range(6)]
        self.bg = pygame.transform.scale(
            pygame.image.load("src/assets/background-day.jpg"), (450, 740)
        )

    def run(self):
        self.screen.blit(self.bg, (0, 0))
        self.draw_ships()
        title = self.font.render("BATTLESOCK", True, COLORS.CYAN)
        self.screen.blit(title, (225 - title.get_width() // 2, 140))

        pygame.draw.rect(self.screen, self.create_button_color, self.create_button)
        pygame.draw.rect(self.screen, COLORS.BLUE, self.create_button, 4)
        pygame.draw.rect(self.screen, COLORS.CYAN, self.join_button)
        pygame.draw.rect(self.screen, COLORS.BLUE, self.join_button, 4)

        self.screen.blit(
            self.create_text, (225 - self.create_text.get_width() // 2, 365)
        )
        if not self.join_hover:
            self.join_code = ""
            self.screen.blit(
                self.join_text, (225 - self.join_text.get_width() // 2, 465)
            )
        else:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return "QUIT"
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_BACKSPACE:
                        self.join_code = self.join_code[:-1]
                    if event.key == pygame.K_RETURN:
                        if len(self.join_code) == 6:
                            return {"category": "JOIN", "payload": self.join_code}
                    elif (t := event.unicode) in __import__("string").ascii_lowercase:
                        self.join_code += t
                    self.join_code = self.join_code[:6]
            if self.join_code == "" or self.join_code == "_":
                self.update_cursor()
                self.join_code = self.cursor
            else:
                self.join_code = self.join_code.strip("_")
            code_text = self.small_font.render(self.join_code, True, COLORS.BLACK)
            self.screen.blit(code_text, (225 - code_text.get_width() // 2, 465))
            if self.invalid_code:
                text = self.small_font.render("Invalid Code", True, COLORS.RED)
                self.screen.blit(text, (230 - text.get_width() // 2, 585))
            elif self.game_taken:
                text = self.small_font.render("Game Occupied", True, COLORS.RED)
                self.screen.blit(text, (230 - text.get_width() // 2, 585))
        m_x, m_y = pygame.mouse.get_pos()
        if self.create_button.collidepoint(m_x, m_y):
            if pygame.mouse.get_pressed(3)[0]:
                return {"category": "CREATE"}
            self.join_hover = False
            self.invalid_code = False
            self.game_taken = False
            self.create_button_color = COLORS.CYAN
        elif self.join_button.collidepoint(m_x, m_y):
            self.join_hover = True
            self.create_button_color = COLORS.CYAN
        else:
            self.join_hover = False
            self.invalid_code = False
            self.game_taken = False
            self.create_button_color = COLORS.CYAN

    # update join_code value
    def update_cursor(self):
        self.blink_count += 1
        if self.blink_count > 15:
            self.cursor = "_" if self.cursor == "" else ""
            self.blink_count = 0

    def reset(self):
        self.join_hover = False
        self.join_code = ""
        self.invalid_code = False
        self.blink_count = 0
        self.cursor = "_"
        self.game_taken = False

    def load_entities(self):
        self.create_text = self.small_font.render("Nouvelle Partie", True, COLORS.BLACK)
        self.join_text = self.small_font.render(
            "Rejoindre une Partie", True, COLORS.BLACK
        )
        self.create_button = pygame.Rect(50, 350, 350, 60)
        self.join_button = pygame.Rect(50, 450, 350, 60)
        self.join_hover = False
        self.join_code = ""
        self.blink_count = 0
        self.cursor = "_"
        self.create_button_color = COLORS.CYAN

    # dessine des bateaux qui passent sur l'écran
    def draw_ships(self):
        self.ships = sorted(self.ships, key=lambda s: s.x)
        for ship in self.ships:
            if any(
                ship != s and ship.y >= s.y and ship.y <= s.y + s.image.get_width()
                for s in self.ships
            ):
                self.ships.remove(ship)
        for ship in self.ships:
            ship.draw(self.screen)
        self.ships = [ship for ship in self.ships if ship.visible]
        while len(self.ships) < 3:
            self.ships.append(Ship())
