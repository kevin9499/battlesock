# coding: utf-8
"""server.py
handle the networking logic for a battleship game.
author: Francois Lavigne Marbach
maintainers: 
    - Raphael Maria
    - Kevin Lefebvre

last modified: 29/11/2021
TODO: split code to clean file
"""
# ---------------------
# ------ imports ------
# built in
import json
import socket
import string
import random
import secrets
from threading import Lock, Thread

# --------------------
# ------ global ------
# création du lock pour le thread
lock = Lock()

# liste les bateaux et leur longueur
SHIPS = {
    "Carrier": 5,
    "Battleship": 4,
    "Cruiser": 3,
    "Submarine": 3,
    "Destroyer": 2,
}

# -------------------
# ------ utils ------
# class utilitaire qui permet de créer une cellule de la map
class Cell:
    def __init__(self, rect, color):
        self.rect = rect
        self.ship = None
        self.color = color
        self.empty = 1
        self.aimed = False
        self.perma_color = None


# créé la grille de jeu. retourne une liste de liste contenant des cells
def create_grid(sx, ex, sy, ey, color, size=35):
    return [
        [Cell((x, y, size, size), color).__dict__ for y in range(sy, ey, size)]
        for x in range(sx, ex, size)
    ]


# ajoute les bateaux a la grille
def add_ships_to_grid():
    grid = create_grid(50, 400, 390, 730, (76, 86, 106))
    for name, size in SHIPS.items():
        while True:
            ship_collision = False
            coords = []
            coord1 = random.randint(0, 9)
            coord2 = random.randint(0, 10 - size)

            if random.choice((True, False)):
                x, y = coord1, coord2
                xi, yi = 0, 1
            else:
                x, y = coord2, coord1
                xi, yi = 1, 0

            for i in range(size):
                new_x = x + (xi * i)
                new_y = y + (yi * i)
                if grid[new_x][new_y]["ship"]:
                    ship_collision = True
                    break
                coords.append((new_x, new_y))
            if not ship_collision:
                break
        for bx, by in coords:
            grid[bx][by]["ship"] = name
    return grid


# --------------------
# ------ server ------
# class qui va gerer la salle de jeux
class Room:
    def __init__(self):
        self.players = []
        self.sent_board = False
        self._id = ""

    # genere et envoie une grille de jeu pour les 2 joueurs presents dans la room.
    # choisie egalement le joueur qui jourera en premier
    # definie l'oposant puis envoie a chaque joueur la carte.
    def send_board(self):
        self.players[0].turn = random.choice((True, False))
        self.players[1].turn = not self.players[0].turn
        self.players[0].layout, self.players[1].layout = (
            add_ships_to_grid(),
            add_ships_to_grid(),
        )
        self.players[0].opponent, self.players[1].opponent = (
            self.players[1],
            self.players[0],
        )
        for player in self.players:
            player.conn.send(
                {
                    "category": "BOARD",
                    "payload": [
                        player.turn,
                        player.layout,
                        [
                            (xi, yi, square["ship"])
                            for xi, x in enumerate(player.opponent.layout)
                            for yi, square in enumerate(x)
                            if square["ship"]
                        ],
                    ],
                }
            )


class NewPlayer:
    def __init__(self, conn, room=None):
        self.conn = conn
        self.room = room


# class qui gere le server
class Server:
    server_addr = "0.0.0.0"
    port = 1234
    address = server_addr, port

    # clean la liste des parties en cours et attend des joueurs
    def __init__(
        self, sock=socket.socket(socket.AF_INET, socket.SOCK_STREAM), is_server=True
    ):
        self.server = sock
        if is_server:
            self.game_list = {}
            self.server.bind(self.address)
            self.wait_for_players()

    def wait_for_players(self):
        self.server.listen()
        while True:
            conn, address = self.server.accept()

            conn = Server(sock=conn, is_server=False)

            # on lock le thread pour éviter une race condition
            # la resource utilisé sera protégé le temps que le thread se termine et se delock
            lock.acquire()
            Thread(
                target=self.proceed_with_connection,
                args=(NewPlayer(conn),),
            ).start()
            lock.release()

    def proceed_with_connection(self, player):
        while True:
            try:
                data = player.conn.receive()

                # si pas de donné envoyé on break
                if not data:
                    break

                # si la game est terminé, on la supprime
                if data["category"] == "OVER":
                    if player.room and player.room._id in self.game_list:
                        del self.game_list[player.room._id]
                    player.room = None

                # on créé la game on CREATE
                elif data["category"] == "CREATE":
                    i = self.generate_token()
                    self.game_list[i] = Room()
                    self.game_list[i]._id = i
                    self.game_list[i].players.append(player)
                    player.room = self.game_list[i]
                    player.conn.send({"category": "ID", "payload": i})

                # si la game a deja 2 joueurs, on revoie un message TAKEN,
                # sinon on accept sa connection et on lui envoie la carte
                # si la clef de connection est invalid, on retourne Invalid
                elif data["category"] == "JOIN":
                    try:
                        if len(self.game_list[data["payload"]].players) == 2:
                            player.conn.send("TAKEN")
                        else:
                            player.room = self.game_list[data["payload"]]
                            self.game_list[data["payload"]].players.append(player)
                            self.game_list[data["payload"]].send_board()
                    except KeyError:
                        player.conn.send("INVALID")

                # enfin, si on recois une data de type Position,
                # alors on envoie a l'adversair les données
                elif data["category"] == "POSITION":
                    player.opponent.conn.send(data)
            except:
                # en cas d'erreur on casse la loop
                break

        # si la boucle se termine, envoie a l'adversaire que la partie
        # est terminé. On catch AttributeError car la game peut se terminer sur une
        # déconnection de l'adversaire.
        try:
            player.opponent.conn.send("END")

        except AttributeError:
            print("Closed Without Pair")

        # on supprime la game de la liste des parties en cours
        if player.room and player.room._id in self.game_list:
            del self.game_list[player.room._id]

        # on ferme la connection
        player.conn.close()

        # on return afin de fermer le thread et release le lock.
        return

    # fonction utilitaire pour recevoir des données
    def receive(self):
        buff = b""
        n = int.from_bytes(self.server.recv(4)[:4], "big")
        while n > 0:
            b = self.server.recv(n)
            buff += b
            n -= len(b)
        return json.loads(buff.decode())

    # fonction utilitaire pour envoyer les données
    def send(self, *data):
        if len(data) == 1:
            data = data[0]
        final_data = b""
        data = json.dumps(data)
        final_data += len(data).to_bytes(4, "big")
        final_data += data.encode()
        try:
            self.server.send(final_data)
        except:
            pass

    # ferme la connection
    def close(self):
        self.server.close()

    # genere un token aléatoire lorsqu'un joueur créé une nouvelle salle.
    def generate_token(self):
        _id = "".join(secrets.choice(string.ascii_lowercase) for i in range(6))
        while _id in self.game_list.keys():
            _id = "".join(secrets.choice(string.ascii_lowercase) for i in range(6))
        return _id


if __name__ == "__main__":
    try:
        print(
            f"server is running on address: {Server.server_addr} and port: {Server.port}"
        )
        Server()
    except KeyboardInterrupt:
        print("User closed server")
        exit()
